import {Component, ViewChild} from '@angular/core';
import {Nav, NavController, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { AuthPage } from '../pages/auth/auth';
import {VoucherPage} from "../pages/voucher/voucher";
export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
    @ViewChild(Nav) nav: any;
    rootPage:any = AuthPage;
    guest:boolean = true;
    public userName: string;
    public userEmail: string;
    appMenuItems: Array<MenuItem>;

    constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
      this.appMenuItems = [
          { title: 'Home', component: HomePage, icon: 'home' },
          { title: 'Requests', component: VoucherPage, icon: 'map' }
      ];
    }

    openPage(page) {
        this.nav.push(page.component);
    }
}

