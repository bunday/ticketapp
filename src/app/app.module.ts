import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { AuthPage } from '../pages/auth/auth';
import { VoucherPage } from '../pages/voucher/voucher';
import {HttpClientModule} from "@angular/common/http";
import {IonicStorageModule,Storage} from "@ionic/storage";
import {VoucherDetailsPage} from "../pages/voucher-details/voucher-details";

@NgModule({
  declarations: [
    MyApp,
    HomePage,
      AuthPage,
      VoucherPage,
      VoucherDetailsPage,
  ],
  imports: [
    BrowserModule,
      HttpClientModule,
    IonicModule.forRoot(MyApp),
      IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
      AuthPage,
      VoucherPage,
      VoucherDetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen, {provide: ErrorHandler, useClass: IonicErrorHandler,deps: [Storage]},

  ]
})
export class AppModule {}
