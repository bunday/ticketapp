import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Storage} from "@ionic/storage";

/**
 * Generated class for the VoucherDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-voucher-details',
  templateUrl: 'voucher-details.html',
})
export class VoucherDetailsPage {

  public voucher;
  public isAdmin;
    public headers = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(public navCtrl: NavController, public navParams: NavParams,public http: HttpClient
        ,public storage: Storage, public loader: LoadingController) {

    this.voucher = this.navParams.get("voucher");
    this.isAdmin = this.navParams.get("isAdmin");
    console.log(this.isAdmin);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherDetailsPage');
  }
  makeApproval(id){
      this.approve(id)
  }
  async approve(id){
      let load = this.loader.create({
          content: "Fetching your Requests... Please wait"
      })
      load.present();
      var token = await this.storage.get("token");
      this.headers = this.headers.append('Authorization','Bearer '+token);
      this.http.post("http://172.20.10.3:8000/api/requests/approve/"+id,{headers: this.headers})
          .toPromise().then((res)=>{
          load.dismissAll();
      })
  }

}
