import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import {
    IonicPage,
    NavController,
    NavParams,
    AlertController,
    MenuController,
    ToastController,
    LoadingController
} from 'ionic-angular';
import { VoucherPage } from '../../pages/voucher/voucher';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Storage} from "@ionic/storage";


/**
 * Generated class for the AuthPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html',
})
export class AuthPage {

    public onLoginForm: FormGroup;
    public onRegisterForm: FormGroup;
    auth: string = "login";
    display: boolean = true;
    error: boolean = false;
    errors:any;
    public headers = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(public http: HttpClient,private _fb: FormBuilder,
                public nav: NavController, public alert: AlertController,
                public menu: MenuController, public toastCtrl: ToastController,
                public loader: LoadingController, public storage: Storage) {
        this.menu.swipeEnable(false);
        this.menu.enable(false);
        this.onLoginForm = this._fb.group({
            email: ['', Validators.compose([
                Validators.required
            ])],
            password: ['', Validators.compose([
                Validators.required
            ])]
        });

        this.onRegisterForm = this._fb.group({
            firstName: ['', Validators.compose([
                Validators.required
            ])],
            midName: ['', Validators.compose([
                Validators.required
            ])],
            lastName: ['', Validators.compose([
                Validators.required
            ])],
            phone: ['', Validators.compose([
                Validators.required
            ])],
            address: ['', Validators.compose([
                Validators.required
            ])],
            email: ['', Validators.compose([
                Validators.required
            ])],
            password: ['', Validators.compose([
                Validators.required
            ])]
        });
    }

    ionViewDidLoad() {
      console.log('ionViewDidLoad AuthPage');
    }

    toggleForm() {
      this.display = !this.display;
    }

    login() {
        this.getToken();
      // this.nav.setRoot(VoucherPage);
    }
    async register() {
        await this.registerUser();
    }
    registerUser(){
        return new Promise((resolve,reject)=>{
            let load = this.loader.create({
                content: "Creating Account... Please wait"
            });
            load.present();
            var data = this.onRegisterForm.value;
            this.http.post("http://172.20.10.3:8000/api/register",data,{headers: this.headers})
                .toPromise().then((res:any)=>{
                load.dismissAll();
                var button = {
                    text: "OK",
                    handler: data => {
                        this.toggleForm()
                    }
                }
                let alert = this.alert.create({
                    title: "Success!",
                    message: res.message+" now proceed to login",
                    buttons: [button]
                })
                alert.present();
                }).catch((err)=>{
                    load.dismissAll();
                var button = {
                    text: "OK"
                }
                    let alert = this.alert.create({
                        title: "Oops!",
                        message: err.error.message,
                        buttons: [button]
                    })
                    this.errors = (<any>Object).values(err.error.data);
                    this.error = true;
                    alert.present();
            })
        })
    }
    getToken(){
        return new Promise((resolve,reject)=>{
            let load = this.loader.create({
                content: "Signing in... Please wait"
            });
            load.present();
            var data = {
                "client_id": 2,
                "client_secret": "5cIVx81b3fC2DcgErzGpWxr7RRY41qNXApEkVeZ5",
                "username":this.onLoginForm.value.email,
                "password":this.onLoginForm.value.password,
                "grant_type":"password"
            }
            this.http.post("http://172.20.10.3:8000/api/login",data,{headers: this.headers})
                .toPromise().then((res:any)=>{
                this.storage.set("token",res.access_token).then(()=>{
                    load.dismissAll();
                    var button = {
                        text: "OK",
                        handler: data => {
                            this.nav.setRoot(VoucherPage);
                        }
                    }
                    let alert = this.alert.create({
                        title: "Success!",
                        message: "Log in Successful",
                        buttons: [button]
                    })
                    alert.present();
                    this.headers = this.headers.append('Authorization','Bearer '+res.access_token);
                    this.http.get("http://172.20.10.3:8000/api/user",{headers: this.headers})
                        .toPromise().then((res)=>{
                            this.storage.set("user",res)
                    })
                })
            }).catch((err)=>{
                load.dismissAll();
                var button = {
                    text: "OK"
                }
                let alert = this.alert.create({
                    title: "Oops!",
                    message: err.error.message,
                    buttons: [button]
                })
                alert.present();
            })
        })
    }

}
