import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Storage} from "@ionic/storage";
import {VoucherDetailsPage} from "../voucher-details/voucher-details";

/**
 * Generated class for the VoucherPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-voucher',
  templateUrl: 'voucher.html',
})
export class VoucherPage {

    public headers = new HttpHeaders().set('Content-Type', 'application/json');
    public vouchers:any;
    public isAdmin:boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public http: HttpClient
              ,public storage: Storage, public loader: LoadingController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad VoucherPage');
    this.setAdmin();
    this.fetchRequests();

  }
  async setAdmin(){
      this.storage.get('user').then((user)=>{
          if(user.category==="ADM") this.isAdmin = true;
      });
  }
  openRequestDetails(request){
    console.log(request)
      this.navCtrl.push(VoucherDetailsPage,{'voucher':request,'isAdmin':this.isAdmin});
  }
  formatStatus(stat){
      var color;
      if(stat==="Approved")
          color = "Secondary";
      else if(stat==="Pending")
          color = "Warning";
      else
          color = "Danger";

      return color;
  }
  async fetchRequests(){
      let load = this.loader.create({
          content: "Fetching your Requests... Please wait"
      })
      load.present();
      var token = await this.storage.get("token");
      this.headers = this.headers.append('Authorization','Bearer '+token);
      this.http.get("http://172.20.10.3:8000/api/requests",{headers: this.headers})
          .toPromise().then((res:any)=>{
            this.vouchers = res.data;
            console.log(this.vouchers);
          this.storage.set("requests",res.data)
          load.dismissAll();
      })
  }

}
